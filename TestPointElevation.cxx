//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2012 Sandia Corporation.
//  Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
//  the U.S. Government retains certain rights in this software.
//
//=============================================================================

#include "vtkCellArray.h"
#include "vtkPoints.h"
#include "vtkXMLPolyDataReader.h"
#include "vtkPolyData.h"
#include "vtkPointData.h"
#include "vtkmDataArray.h"
#include "PointElevation.h"

#include <vtkm/cont/Initialize.h>

int main(int argc, char* argv[])
{
  vtkm::cont::Initialize(argc, argv);
  //get all data from the file
  vtkXMLPolyDataReader* reader = vtkXMLPolyDataReader::New();
  reader->SetFileName("data/cow.vtp");
  reader->Update();
  vtkPolyData* polydata = reader->GetOutput();

  vtkNew<PointElevation> elevation;
  elevation->ForceVTKmOn();
  elevation->SetInputData(polydata);
  elevation->SetLowPoint(0, -4, 0);
  elevation->SetHighPoint(0, 4, 0);
  elevation->SetScalarRange(-4, 4);
  elevation->Update();

  std::cout << "Hello!!!!!" << std::endl;
  vtkmDataSet* output = elevation->GetOutput();
  std::cout << "Num of Arrays : " << output->GetPointData()->GetNumberOfArrays() << std::endl;
  {
    std::cout << "Point data" << std::endl;
    auto pointData = output->GetPoints();
    auto pointers = ((vtkmDataArray<float>*)pointData)->GetArrayInformation();
    std::cout << pointers.NumPointers << std::endl;
  }
  {
    std::cout << "Field data" << std::endl;
    auto fieldData = output->GetPointData()->GetArray("elevation");
    auto pointers = ((vtkmDataArray<float>*)fieldData)->GetArrayInformation();
    std::cout << pointers.NumPointers << std::endl;
  }
}
