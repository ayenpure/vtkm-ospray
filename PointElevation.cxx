//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2012 Sandia Corporation.
//  Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
//  the U.S. Government retains certain rights in this software.
//
//=============================================================================
#include "PointElevation.h"
//#include "vtkmConfigFilters.h"

#include "vtkCellData.h"
#include "vtkDataSet.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkPointData.h"
#include "vtkUnstructuredGrid.h"

#include "vtkmlib/ArrayConverters.h"
#include "vtkmlib/DataSetConverters.h"

//#include "vtkmFilterPolicy.h"

#include <vtkm/cont/RuntimeDeviceTracker.h>
#include <vtkm/cont/kokkos/DeviceAdapterKokkos.h>

#include <vtkm/io/VTKDataSetReader.h>
#include <vtkm/io/VTKDataSetWriter.h>

#include <vtkm/filter/field_transform/PointElevation.h>

vtkStandardNewMacro(PointElevation);

//------------------------------------------------------------------------------
PointElevation::PointElevation()
{
  this->LowPoint[0] = 0.0;
  this->LowPoint[1] = 0.0;
  this->LowPoint[2] = 0.0;

  this->HighPoint[0] = 0.0;
  this->HighPoint[1] = 0.0;
  this->HighPoint[2] = 1.0;

  this->ScalarRange[0] = 0.0;
  this->ScalarRange[1] = 1.0;
}

//------------------------------------------------------------------------------
PointElevation::~PointElevation() = default;

//------------------------------------------------------------------------------
int PointElevation::RequestData(
  vtkInformation* request, vtkInformationVector** inputVector, vtkInformationVector* outputVector)
{
  auto& tracker = vtkm::cont::GetRuntimeDeviceTracker();
  tracker.ForceDevice(vtkm::cont::DeviceAdapterTagKokkos{});

  std::cout << "Executing This Filter 1" << std::endl;
  // Get the input and output data objects.
  vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation* outInfo = outputVector->GetInformationObject(0);

  vtkDataSet* input = vtkDataSet::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkmDataSet* output = vtkmDataSet::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

  std::cout << "Executing This Filter 2" << std::endl;
  // Check the size of the input.
  vtkIdType numPts = input->GetNumberOfPoints();
  if (numPts < 1)
  {
    vtkDebugMacro("No input!");
    return 1;
  }

  try
  {
    std::cout << "Executing This Filter 3" << std::endl;
    // Convert the input dataset to a vtkm::cont::DataSet
    auto in = tovtkm::Convert(input, tovtkm::FieldsFlag::Points);
    //vtkm::io::VTKDataSetReader reader("cow.vtk");
    //vtkm::cont::DataSet in = reader.ReadDataSet();

    std::cout << "Executing This Filter 4" << std::endl;

    // Setup input
    vtkm::filter::field_transform::PointElevation filter;
    filter.SetLowPoint(this->LowPoint[0], this->LowPoint[1], this->LowPoint[2]);
    filter.SetHighPoint(this->HighPoint[0], this->HighPoint[1], this->HighPoint[2]);
    filter.SetRange(this->ScalarRange[0], this->ScalarRange[1]);
    filter.SetOutputFieldName("elevation");
    filter.SetUseCoordinateSystemAsField(true);

    std::cout << "Executing This Filter 5" << std::endl;

    in.PrintSummary(std::cout);
    auto result = filter.Execute(in);

    std::cout << "Executing This Filter 6" << std::endl;
    // Convert the result back
    //vtkDataArray* resultingArray = fromvtkm::Convert(result.GetField("elevation"));
    //if (resultingArray == nullptr)
    //{
    //  vtkErrorMacro(<< "Unable to convert result array from VTK-m to VTK");
    //  return 0;
    //}
    output->SetUseVtkmArrays(true);
    output->SetVtkmDataSet(result);
//    output->GetPointData()->AddArray(resultingArray);
//    output->GetPointData()->SetActiveScalars("elevation");
//    resultingArray->FastDelete();
  }
  catch (const vtkm::cont::Error& e)
  {
    vtkErrorMacro(<< "VTK-m error: " << e.GetMessage());
  }
  return 1;
}

//------------------------------------------------------------------------------
void PointElevation::PrintSelf(std::ostream& os, vtkIndent indent)
{
  os << indent << "Low Point: (" << this->LowPoint[0] << ", " << this->LowPoint[1] << ", "
     << this->LowPoint[2] << ")\n";
  os << indent << "High Point: (" << this->HighPoint[0] << ", " << this->HighPoint[1] << ", "
     << this->HighPoint[2] << ")\n";
  os << indent << "Scalar Range: (" << this->ScalarRange[0] << ", " << this->ScalarRange[1]
     << ")\n";
}
