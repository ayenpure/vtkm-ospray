//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================

#include <vtkm/cont/Initialize.h>

#include <vtkm/io/VTKDataSetReader.h>
#include <vtkm/io/VTKDataSetWriter.h>
#include <vtkm/filter/field_transform/PointElevation.h>

#include "level_zero/ze_api.h"
#include <omp.h>
extern "C" void *omp_target_get_context(int);

namespace detail
{

enum class ALLOC
{
  HOST = 0,
  DEVICE
};

struct MetaData
{
  const void* Device;
  const void* Context;
  size_t      NumPointers;
  std::vector<const void*> Pointers;
  std::vector<detail::ALLOC> Allocs;
};

struct MetaDataFunctor
{
  template <typename Device, typename T, typename Storage>
  VTKM_CONT bool operator()(Device,
                            const vtkm::cont::ArrayHandle<T, Storage>& fielddata,
                            detail::MetaData& metadata)
  {
    auto  ompdevice  = omp_get_default_device();
    auto  ompcontext = omp_target_get_context(ompdevice);
    std::cout << "Got device at : " << &ompdevice << std::endl;
    std::cout << "Got context at : " << &ompcontext << std::endl;
    auto buffers = fielddata.GetBuffers();
    auto numbuff = fielddata.GetNumberOfBuffers();
    vtkm::cont::Token token;
    std::vector<const void*> pointers;
    std::vector<detail::ALLOC> allocs;
    for(int i = 0; i < numbuff; i++)
    {
      const void* pointer;
      ALLOC alloc;
      if(buffers[i].IsAllocatedOnDevice(Device{}))
      {
        alloc = ALLOC::DEVICE;
        pointer = buffers[i].ReadPointerDevice(Device{}, token);
      }
      else if(buffers[i].IsAllocatedOnHost())
      {
        alloc = ALLOC::HOST;
        pointer = buffers[i].ReadPointerHost(token);
      }
      printf("Pointer is pointing to %p\n", pointer);
      pointers.push_back(pointer);
      allocs.push_back(alloc);
    }
    metadata.Device      = (const void*)&ompdevice;
    metadata.Context     = (const void*)&ompcontext;
    metadata.NumPointers = pointers.size();
    metadata.Pointers    = pointers;
    metadata.Allocs      = allocs;
    return true;
  }
};

struct ExtractPointsData
{
  template <typename T, typename S>
  VTKM_CONT void operator()(const vtkm::cont::ArrayHandle<T, S>& array) const
  {
    std::cout << "Getting Coords Data" << std::endl;
    detail::MetaData metadata;
    bool found = vtkm::cont::TryExecute(detail::MetaDataFunctor{}, array, metadata);
  }
};

}

//#include <vtkNew.h>
//#include <vtkmDataSet.h>
//#include <vtkmDataArray.h>
//#include <vtkPointData.h>
/*
struct ArrayConverter
{
public:
  mutable vtkDataArray* Data;

  ArrayConverter()
    : Data(nullptr)
  {
  }

  // CastAndCall always passes a const array handle. Just shallow copy to a
  // local array handle by taking by value.
  template <typename T, typename S>
  void operator()(const vtkm::cont::ArrayHandle<T, S>& handle) const
  {
    this->Data = make_vtkmDataArray(handle);
  }
};
*/



int main(int argc, char** argv)
{
  vtkm::cont::Initialize(argc, argv);
  //auto& tracker = vtkm::cont::GetRuntimeDeviceTracker();

  //vtkm::cont::ScopedRuntimeDeviceTracker stracker(tracker,
  //                                                vtkm::cont::RuntimeDeviceTrackerMode::Force);

  vtkm::io::VTKDataSetReader reader("data/cow.vtk");
  vtkm::cont::DataSet dataSet = reader.ReadDataSet();

  vtkm::Vec3f LowPoint(0, -4, 0);
  vtkm::Vec3f HighPoint(0, 4, 0);
  vtkm::Vec2f ScalarRange(-4, 4);

  vtkm::filter::field_transform::PointElevation filter;
  filter.SetLowPoint(LowPoint[0], LowPoint[1], LowPoint[2]);
  filter.SetHighPoint(HighPoint[0], HighPoint[1], HighPoint[2]);
  filter.SetRange(ScalarRange[0], ScalarRange[1]);
  filter.SetOutputFieldName("elevation");
  filter.SetUseCoordinateSystemAsField(true);

  dataSet.PrintSummary(std::cout);

  auto result = filter.Execute(dataSet);

  vtkm::cont::UnknownArrayHandle cdata = result.GetCoordinateSystem().GetData();
  cdata.CastAndCallForTypes<VTKM_DEFAULT_TYPE_LIST, VTKM_DEFAULT_STORAGE_LIST>(detail::ExtractPointsData{});

  int numFields = result.GetNumberOfFields();
  std::cout << "Num of Arrays : " << numFields << std::endl;
  for(int i = 0; i < numFields; i ++)
  {
    std::cout << result.GetField(i).GetName() << std::endl;
  }

  vtkm::cont::ArrayHandle<double> fielddata;
  result.GetField(1).GetData().AsArrayHandle(fielddata);

  std::cout << result.GetField(1).GetData().GetNumberOfValues() << std::endl;

  detail::MetaData metadata;
  bool found = vtkm::cont::TryExecute(detail::MetaDataFunctor{}, fielddata, metadata);

  std::cout << "Found metadata : " << found << std::endl;
  std::cout << "Found metadata : " << metadata.NumPointers << std::endl;
  std::cout << "Found metadata : " << metadata.Pointers[0] << std::endl;


  /*  auto buffers = fielddata.GetBuffers();
  std::cout << "Is allocated on host?   : "
            << buffers[0].IsAllocatedOnHost() << std::endl;
  std::cout << "Is allocated on device? : "
            << buffers[0].IsAllocatedOnDevice(vtkm::cont::DeviceAdapterTagKokkos{}) << std::endl;

  vtkm::cont::Token token;
  const void* dpointer = buffers[0].ReadPointerDevice(vtkm::cont::DeviceAdapterTagKokkos{}, token);
  printf("Address of device pointer is %p\n", (void *)dpointer);
*/
/*  ArrayConverter aConverter;
  const vtkm::cont::Field& field = result.GetField(0);
  try
  {
    vtkDataArray* data;
    vtkm::cont::CastAndCall(field.GetData(), aConverter);
    data = aConverter.Data;
  }
  catch (vtkm::cont::Error&)
  {
    std::cout << "How did we end up here!!!!" << std::endl;
  }*/
/*  vtkNew<vtkmDataSet> _temp;
  _temp->SetUseVtkmArrays(true);
  _temp->SetVtkmDataSet(result);
  std::cout << "Num of Arrays : " << _temp->GetPointData()->GetNumberOfArrays() << std::endl;
  auto fieldData = _temp->GetPointData()->GetArray("elevation");
  auto pointers = ((vtkmDataArray<float>*)fieldData)->GetDeviceArrays();
  std::cout << pointers.size() << std::endl;
*/
  //std::cout << "Temp 1 : " << _temp->GetNumberOfPoints() << std::endl;
  //std::cout << "Temp 2 : " << _temp->GetNumberOfCells() << std::endl;
  //  vtkm::io::VTKDataSetWriter writer("output.vtk");
  //  writer.WriteDataSet(result);
  //
/*  int OMPD = omp_get_default_device();
  //cl_context OMPC = static_cast<cl_context>(omp_target_get_context(OMPD));
  std::cout << "We have OpenMP " << _OPENMP << " " << OMPD << "\n";*/
}

